package Base;

import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class BaseTest {
    public static WebDriver driver;
    public static Actions action;
    static String url = "https://www.teb.com.tr";

    @BeforeScenario
    public void senaryoBaslangic(){
        System.out.println("Senaryo baslatiliyor");
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.manage().window().maximize();
        driver.get(url);
    }

    @AfterScenario
    public void senaryoSonu(){
        driver.quit();
        System.out.println("Senaryo basarili");
    }
}
