package Steps;

import Base.BaseTest;
import com.thoughtworks.gauge.Step;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Set;

public class BaseSteps extends BaseTest {

    public WebElement findElement(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }

    @Step("<key> elementi var mi kontrol et")
    public void elementKontrol(String key) {
        try {
            findElement(key);
        } catch (Exception e) {
            Assert.fail("Element bulunamadı.");
        }
    }

    @Step("<elementKey> elementi ustune gel ve ac")
    public void ustuneGelAc(String elementKey) throws InterruptedException {
        Thread.sleep(2000);
        action.moveToElement(findElement(elementKey)).click().build().perform();
        Thread.sleep(2000);
    }

    @Step("<elementKey> elementi ustune gel")
    public void ustuneGel(String elementKey) throws InterruptedException {
        Thread.sleep(2000);
        action.moveToElement(findElement(elementKey)).build().perform();
        Thread.sleep(2000);
    }

    @Step("Kontrolu yeni sekmeye devret")
    public void tabKontrolDegisimi() {
        Set<String> tab_handles = driver.getWindowHandles();
        int number_of_tabs = tab_handles.size();
        int new_tab_index = number_of_tabs-1;
        driver.close();
        System.out.println("Eski sekme kapatildi");
        driver.switchTo().window(tab_handles.toArray()[new_tab_index].toString());
        System.out.println("Yeni sekmeye kontrol verildi");
    }
}
